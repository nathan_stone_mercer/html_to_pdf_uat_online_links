<script runat="server" language="JavaScript">
	Platform.Load("core", "1");

	// Random query string brought in from the URL for security purposes
	if (Platform.Request.GetQueryStringParameter("id") == "d1f2cdacb4c01ad56005c7dc8d6e2dea8a5ee745") {

		// Get the payload posted to this page
		var obj = Platform.Function.ParseJSON(Platform.Request.GetPostData("UTF-8"));
		var inArgs = obj.inArguments;

		// structure the triggered send payload
		var tsObj = [];
		var eventInstanceID, triggeredSendExternalKey, email, subscriberKey;

		// iterate through the inArguments and construct the ts payload
		for (var i = 0; i < inArgs.length; i++) {
			for (var p in inArgs[i]) {
				// Make sure the below values are not null
				if (inArgs[i][p]) {
					var tempObj = {};

					switch (p) {
						case 'TestRecord':
							tempObj.Name = 'test_send';
							tempObj.Value = inArgs[i][p];
							break;
						case 'TriggeredSendExternalKey':
							triggeredSendExternalKey = inArgs[i][p];
							tempObj.Name = p;
							tempObj.Value = inArgs[i][p];
							break;
						case 'Email':
							email = inArgs[i][p];
							tempObj.Name = p;
							tempObj.Value = inArgs[i][p];
							break;
						case 'cloupra__Person__c':
							subscriberKey = inArgs[i][p];
							tempObj.Name = p;
							tempObj.Value = inArgs[i][p];
							break;
						default:
							tempObj.Name = p;
							tempObj.Value = inArgs[i][p];
					}

					tsObj.push(tempObj);
				}
			}
		}

		//add the stringified json payloads
		var tempObj = {
			Name: 'template',
			Value: Platform.Function.Lookup('SiaB Incoming Correspondence Data', 'template', 'EventInstanceID', eventInstanceID)
		};
		tsObj.push(tempObj);

		// Write(Stringify(tsObj));


	// 	if (inArgs) {
	// 		// initiate the WSProxy object
	// 		var prox = new Script.Util.WSProxy();

	// 		var ts = {
	// 			TriggeredSendDefinition: {
	// 				CustomerKey: triggeredSendExternalKey
	// 			},
	// 			Subscribers: [{
	// 				EmailAddress: email,
	// 				SubscriberKey: subscriberKey,
	// 				Attributes: tsObj
	// 			}] //END Subscriber Property
	// 		} //END ts Object Property

	// 		// execute trigger send
	// 		var tsStatus = prox.createItem("TriggeredSend", ts);

	// 		// Data Extension used for logging - use External keyValue
	// 		var DE = DataExtension.Init("SIABCustomLog");
	// 		// Construct logging object
	// 		var logObjRow = [{
	// 			ContactKey: obj.keyValue,
	// 			JourneyVersionID: obj.journeyId,
	// 			ActivityId: obj.activityId,
	// 			ActivityObjectID: obj.activityObjectID,
	// 			DefinitionInstanceId: obj.definitionInstanceId,
	// 			ActivityInstanceId: obj.activityInstanceId,
	// 			inArguments: Stringify(inArgs),
	// 			TriggeredSendPayload: Stringify(ts),
	// 			TriggeredSendStatus: tsStatus.Status,
	// 			CompleteJourneyObjectData: Stringify(obj),
	// 			TriggeredSendResults: Stringify(tsStatus.Results)
	// 		}];

	// 		var logStatus = DE.Rows.Add(logObjRow);

	// 		Write(Stringify({
	// 			"TsSendStatus": tsStatus.Status,
	// 			"TsResults": tsStatus.Results[0].StatusMessage,
	// 			"TsErrorCode": tsStatus.Results[0].ErrorCode
	// 		}));

	// 	} else {
	// 		Write(Stringify({
	// 			"Error": 'No payload constructed.'
	// 		}));

	// 	} //END tsObj check
	// } else {
	// 	Write('Page not accessible.')
	// } //END id param check
</script>