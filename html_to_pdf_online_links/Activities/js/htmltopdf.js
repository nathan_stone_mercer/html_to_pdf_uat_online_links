define( function( require ) {

    'use strict';

    var Postmonger = require( 'postmonger' );
    var $ = require( 'jquery' );

    var connection = new Postmonger.Session();
    var toJbPayload = {};
    var step = 1;
    var tokens;
    var endpoints;

    $(window).ready(onRender);

    connection.on('initActivity', function(payload) {
        if (payload) {
            toJbPayload = payload;
        }
          gotoStep(step);
    });

    connection.on('requestedTokens', function(data) {
        if( data.error ) {
            console.error( data.error );
        } else {
            tokens = data;
            console.log("tokens",data);
        }
    });

    connection.on('requestedEndpoints', function(data) {
        if( data.error ) {
            console.error( data.error );
        } else {
            endpoints = data;
            console.log("endpoints",endpoints);
        }
    });

    connection.on('clickedNext', function() {
        step++;
        gotoStep(step);
        connection.trigger('ready');
    });

    connection.on('clickedBack', function() {
        step--;
        gotoStep(step);
        connection.trigger('ready');
    });

    function onRender() {
        connection.trigger('ready');
        connection.trigger('requestTokens');
        connection.trigger('requestEndpoints');
    };

    function gotoStep(step) {
        $('.step').hide();
        switch(step) {
            case 1:
                $('#step1').show();
                connection.trigger('updateButton', { button: 'next', text: 'next', enabled: true });
                connection.trigger('updateButton', { button: 'back', visible: false });
                break;
            case 2: // Only 2 steps, so the equivalent of 'done' - send off the payload
                save();
                break;
        }
    };

    function save() {
        // get the Key field from the Interactions API call for the journey
        var eventDefinitionKey = "DEAudience-23f8e160-978a-66b6-22f7-71b88c9abdbc";

        // this will be sent into the custom activity body within the inArguments array
        toJbPayload['arguments'].execute.inArguments = [
          {"SubscriberKey":"{{Event."+ eventDefinitionKey + ".SubscriberKey}}"},
          {"Source__c":"{{Event."+ eventDefinitionKey + ".Source__c}}"},
          {"Type__C_Desc":"{{Event."+ eventDefinitionKey + ".Type__C_Desc}}"},
          {"Communication_Name":"{{Event."+ eventDefinitionKey + ".Communication_Name}}"},
          {"TemplateCode":"{{Event."+ eventDefinitionKey + ".TemplateCode}}"},
          {"Interaction_Name":"{{Event."+ eventDefinitionKey + ".Interaction_Name}}"},
          {"GUID":"{{Event."+ eventDefinitionKey + ".GUID}}"},
          {"SalesforceAccountID":"{{Event."+ eventDefinitionKey + ".SalesforceAccountID}}"},
          {"SalesforcePersonID":"{{Event."+ eventDefinitionKey + ".SalesforcePersonID}}"},
          {"DE_Key":"{{Event."+ eventDefinitionKey + ".DE_Key}}"},
          {"DE_RowID":"{{Event."+ eventDefinitionKey + ".DE_RowID}}"},
          {"EmailName":"{{Event."+ eventDefinitionKey + ".EmailName}}"},
          {"TS_Key":"{{Event."+ eventDefinitionKey + ".TS_Key}}"}
        ];

        toJbPayload.metaData.isConfigured = true;  //this is required by JB to set the activity as Configured
        connection.trigger('updateActivity', toJbPayload);
    };

});
